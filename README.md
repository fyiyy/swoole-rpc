composer require fyiyy/swoole-rpc

### 使用

#### 服务端使用

```php

use Fy\Rpc\RpcServer;


class test
{
    public function index($param)
    {
        return $param;
    }
}

class server
{

    public function index($param)
    {
        return $param;
    }

    public function test()
    {
        include_once 'vendor/autoload.php';

        $config = [
            'host' => '0.0.0.0',
            'port' => 9502
        ];

        $RpcServer = new RpcServer($config);

        $RpcServer->add($this, 'index');
        $RpcServer->add(new test(), 'index');

        $RpcServer->addBatch(
            ['obj' => $this, 'method' => 'sayHello'],
            ['obj' => $this, 'method' => 'sayHello1']
        );
        $RpcServer->start();
    }


    /**
     * @param string $name
     * @return string
     */
    public function sayHello(string $name): string
    {
        return 'Server Say ' . $name;
    }

    public function sayHello1(string $name): string
    {
        return 'Server Say11 ' . $name;
    }

}

$obj = new server();
$obj->test();

```

#### 客户端使用

```php

use Fy\Rpc\RpcClient;


class client
{

    public function test()
    {
        include_once 'vendor/autoload.php';

        $config = [
            'host' => '192.168.0.5',
            'port' => 9502,
            'timeOut' => 5
        ];

        $client = new RpcClient($config);
        $result = $client->setClass('server')->index(['code' => 1, 'msg' => 'server']);
        var_dump($result);
        $result = $client->setClass('server')->sayHello('Swoole');
        var_dump($result);
        $result = $client->setClass('test')->index(['code' => 1, 'msg' => 'test']);
        var_dump($result);
        $result = $client->setClass('server')->index(['code' => 1, 'msg' => 'server']);
        var_dump($result);
        $result = $client->setClass('server')->sayHello1('111111');
        var_dump($result);
    }

}

$obj = new client();

$obj->test();

```