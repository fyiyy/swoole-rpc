<?php


use Fy\Rpc\RpcServer;
use app\test;

class server
{

    public function index($param)
    {
        return $param;
    }

    public function test()
    {
        include_once 'vendor/autoload.php';
        include_once 'test.php';

        $config = [
            'host' => '0.0.0.0',
            'port' => 9502
        ];

        $RpcServer = new RpcServer($config);

        $RpcServer->add($this, 'index');
        $RpcServer->add(new test(), 'index');

        $RpcServer->addBatch(
            ['obj' => $this, 'method' => 'sayHello'],
            ['obj' => $this, 'method' => 'sayHello1']
        );
        $RpcServer->start();
    }


    /**
     * @param string $name
     * @return string
     */
    public function sayHello(string $name): string
    {
        return 'Server Say ' . $name;
    }

    public function sayHello1(string $name): string
    {
        return 'Server Say11 ' . $name;
    }

}

$obj = new server();
$obj->test();
