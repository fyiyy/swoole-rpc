<?php

namespace Fy\Rpc;

use Swoole\Server;

class RpcServer
{
    /**
     * 服务属性
     * @var Server
     */
    private Server $server;

    /**
     * 对象属性
     * @var array
     */
    private array $objArr = [];

    /**
     * 主机地址属性
     * @var string
     */
    private $host = '127.0.0.1';

    /**
     * 监听端口属性
     * @var int
     */
    private int $port = 9501;

    /**
     * 配置属性
     * @var array
     */
    private array $config = [
        'reactor_num' => 2,     // 线程数
        'worker_num' => 4,     // 进程数
        'backlog' => 128,   // 设置Listen队列长度
        'max_request' => 50,    // 每个进程最大接受请求数
        'dispatch_mode' => 1,     // 数据包分发策略
    ];

    /**
     * 构造方法
     * RpcServer constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        if (isset($config['host'])) {
            $this->host = $config['host'];
            unset($config['host']);
        }
        if (isset($config['port'])) {
            $this->port = $config['port'];
            unset($config['port']);
        }

        $this->config = array_merge($this->config, $config);

        // 启动服务进行监听
        $this->server = new Server($this->host, $this->port, SWOOLE_PROCESS, SWOOLE_SOCK_TCP);
        $this->server->set($this->config);
        $this->server->on('Receive', [$this, 'onReceive']);

        echo "Service started at port {$this->port} on {$this->host} ...\n";
    }

    /**
     * 监听消息
     * @param $server
     * @param $fd
     * @param $from_id
     * @param $data
     */
    public function onReceive($server, $fd, $from_id, $data)
    {
        $data = json_decode($data, true);
        foreach ($this->objArr as $key => $value) {
            if (empty($data['params'])) {
                $data['params'] = $value['params'];
            }
            if (get_class($value['obj']) == $data['className'] && $value['method'] == $data['method']) {
                // 调用方法，返回结果
                $result = call_user_func_array([$value['obj'], $data['method']], $data['params']);
                $server->send($fd, json_encode($result));
            }
        }
    }


    /**
     * 添加服务端方法
     * @param object $obj 对象名
     * @param string $method 方法名
     * @param array $params 参数
     */
    public function add(object $obj, string $method, $params = [])
    {
        $arr = [
            'obj' => $obj,
            'method' => $method,
            'params' => $params
        ];
        array_push($this->objArr, $arr);
    }


    /**
     * 批量添加服务端方法
     * @param array $params
     * ['obj' => obj1, 'method' => 'method1'], ['obj' => obj2, 'method' => 'method2']
     */
    public function addBatch(array ...$params)
    {
        foreach ($params as $value) {
            if (!isset($value['params'])) {
                $value['params'] = [];
            }
            if (!is_array($value['params'])) {
                $value['params'] = [$value['params']];
            }
            $arr = [
                'obj' => $value['obj'],
                'method' => $value['method'],
                'params' => $value['params']
            ];
            array_push($this->objArr, $arr);
        }
    }


    /**
     * 启动监听
     */
    public function start()
    {
        $this->server->start();
    }

}