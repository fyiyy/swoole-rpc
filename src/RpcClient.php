<?php

namespace Fy\Rpc;

use Swoole\Client;

class RpcClient
{
    /**
     * 客户端服务
     * @var Client
     */
    private Client $client;

    /**
     * 主机地址属性
     * @var string
     */
    private string $host = '127.0.0.1';

    /**
     * 监听端口属性
     * @var int
     */
    private int $port = 9501;

    /**
     * 配置属性
     * @var array
     */
    private array $config = [
        'timeOut' => -1
    ];

    /**
     * 指定调用的类名属性
     * @var string
     */
    private string $className = '';

    /**
     * 构造方法
     * RpcServer constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->config = array_merge($this->config, $config);
        $this->client = new Client(SWOOLE_SOCK_TCP);
    }

    /**
     * 调用远端方法
     * @param $method
     * @param $params
     * @return false|mixed
     */
    private function call($method, $params)
    {
        if (isset($this->config['host'])) {
            $this->host = $this->config['host'];
            unset($this->config['host']);
        }
        if (isset($this->config['port'])) {
            $this->port = $this->config['port'];
            unset($this->config['port']);
        }

        if (!$this->client->connect($this->host, $this->port, $this->config['timeOut'])) {
            return false;
        }

        // 构造请求参数
        $data = [
            'className' => $this->className,
            'method' => $method,
            'params' => $params,
        ];

        // 发送请求
        $this->client->send(json_encode($data));

        // 接收响应
        $result = $this->client->recv();

        // 关闭连接
        $this->client->close();

        return json_decode($result, true);
    }

    /**
     * 指定调用类名
     * @param string $name
     * @return $this
     */
    public function setClass($name = ''): RpcClient
    {
        $this->className = $name;
        return $this;
    }


    /**
     * @param $name
     * @param $arguments
     * @return false|mixed
     */
    public function __call($name, $arguments)
    {
        return $this->call($name, $arguments);
    }
}
